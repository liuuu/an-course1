import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
import { CribsService } from '../services/cribs.service';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-crib-listing',
  templateUrl: './crib-listing.component.html',
  styleUrls: ['./crib-listing.component.css']
})
export class CribListingComponent implements OnInit {

  cribs: Array<any>;
  error: string;
  sortFields: Array<string> = [
    "address",
    "area",
    "bathrooms",
    "bedrooms",
    "type",
    "price"
  ];
  sortField: string = 'price';
  sortDirection: string = 'asc';

  constructor(
    private http: Http, 
    private cribService: CribsService,
    private utilService: UtilService
  ) { }

  ngOnInit() {
    // this.http.get('data/cribs.json')
    // .map(res => res.json())
    // .subscribe(
    //   data => this.cribs = data,
    //   error => this.error = error.statusText
    // )

    this.cribService.getAllCribs()
      .subscribe(
      data => this.cribs = data,
      error => this.error = error.statusText
    )

    this.cribService.newCribSubject.subscribe(data => {
      console.log(1);
      console.log(data)
      this.cribs =  [data, ...this.cribs]
    
    })

  }

}
